import math
import random

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 14

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1,
    'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

WORDLIST_FILENAME = "words.txt"


def load_words():
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def get_frequency_dict(sequence: str) -> dict:
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x, 0) + 1
    return freq


def get_word_score(word: str, n: int) -> int:
    return sum([SCRABBLE_LETTER_VALUES.get(x, 0) for x in word.lower()]) * max(7 * len(word) - 3 * (n - len(word)), 1)


def display_hand(hand: dict):
    for letter in hand.keys():
        for j in range(hand[letter]):
            print(letter, end=' ')
    print()


def deal_hand(n: int) -> dict:
    hand = {'*': 1}
    num_vowels = int(math.ceil(n / 3)) - 1

    for i in range(num_vowels):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1

    for i in range(num_vowels + 1, n):
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1

    return hand


def update_hand(hand: dict, word: str):
    word_freq = get_frequency_dict(word.lower())
    return {letter: freq - word_freq.get(letter, 0) for letter, freq in hand.items()
            if freq - word_freq.get(letter, 0) > 0}


def check_hand(hand, word_hand):
    return all(hand.get(letter, 0) >= freq for letter, freq in word_hand)


def is_valid_word(word: str, hand: dict, word_list: list):
    word = word.lower()
    word_hand = get_frequency_dict(word).items()
    if not check_hand(hand, word_hand):
        # мы используем слово символов для которого у нас нет в руке
        return False
    if '*' not in word:
        return word in word_list
    flag = False
    for other_word in (word.replace('*', letter) for letter in VOWELS):
        if other_word in word_list:
            if check_hand(hand, get_frequency_dict(other_word).items()):
                # мы могли не использовать * значит ошиблись
                return False
            else:
                flag = True
    return flag


def calculate_handlen(hand: dict):
    return sum(hand.values())


def play_hand(hand: dict, word_list: list):
    score = 0
    while calculate_handlen(hand) > 0:
        print("Current Hand: ", end='')
        display_hand(hand)
        word = input('Enter word, or "!!" to indicate that you are finished: ').strip()
        if word == '!!':
            break
        if is_valid_word(word, hand, word_list):
            current_score = get_word_score(word, calculate_handlen(hand))
            score += current_score
            print(f'"{word}" earned {current_score} points. Total: {score} points')
        else:
            print("That is not a valid word. Please choose another word.")
        hand = update_hand(hand, word)
        print()
    if calculate_handlen(hand) == 0:
        print("Ran out of letters.", end=' ')
    print(f"Total score for this hand: {score} points")
    return score


def substitute_hand(hand, letter):
    new_hand = hand.copy()
    if letter in hand:
        del new_hand[letter]
        new_hand[random.choice(list((set(VOWELS) | set(CONSONANTS)) - set(hand.keys())))] = \
            random.randint(1, HAND_SIZE - calculate_handlen(new_hand))
    return new_hand


def play_game(word_list):
    hand_count = input("Enter total number of hands: ")
    hand = deal_hand(HAND_SIZE)
    score = 0
    while hand_count != 0:
        print("Current Hand: ", end='')
        display_hand(hand)
        if input("Would you like to substitute a letter? ").strip() == "yes":
            new_hand = substitute_hand(hand, input("Which letter would you like to replace: ").strip())
        else:
            new_hand = hand.copy()
        score_hand = play_hand(new_hand, word_list)
        print("----------")
        if input("Would you like to replay the hand? ").strip() != "yes":
            hand = deal_hand(HAND_SIZE)
            score += score_hand
            hand_count -= 1
    print("Total score over all hands:", score)


if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)
